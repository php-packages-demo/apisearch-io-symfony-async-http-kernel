# apisearch-io/[symfony-async-http-kernel](https://packagist.org/packages/apisearch-io/symfony-async-http-kernel)

Symfony Async Kernel Adapter

[![PHPPackages Rank](http://phppackages.org/p/apisearch-io/symfony-async-http-kernel/badge/rank.svg)
](http://phppackages.org/p/apisearch-io/symfony-async-http-kernel)
[![PHPPackages Referenced By](http://phppackages.org/p/apisearch-io/symfony-async-http-kernel/badge/referenced-by.svg)
](http://phppackages.org/p/apisearch-io/symfony-async-http-kernel)

## Official documentation
* [*Symfony and ReactPHP Series — Chapter 5*
  ](https://medium.com/@apisearch/symfony-and-reactphp-series-chapter-5-1f2f355cf302).
  2019 Apisearch

## Other strategy to adapt Symfony kernel
* [*Super Speed Symfony - ReactPHP*
  ](https://gnugat.github.io/2016/04/13/super-speed-sf-react-php.html).
  2016 Loïc Faugeron